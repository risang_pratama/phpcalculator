<?php


namespace Jakmall\Recruitment\Calculator\History;


use Jakmall\Recruitment\Calculator\DBConnection;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;
use Jakmall\Recruitment\Calculator\Model\History;

class HistoryStorageService implements CommandHistoryManagerInterface
{
    private $file;

    public function __construct()
    {
        $this->file = __DIR__."/../../database/storage/calculator.json";
        new DBConnection();
    }

    /**
     * @inheritDoc
     */
    public function findAll($driver, $command): array
    {
        $columns = ['command', 'description', 'result', 'output', 'created'];
        if($driver == "database") {
            $data = $this->findFromDatabase($columns, $command);
        } else {
            $data = $this->findFromFile($columns, $command);
        }
        return $data;
    }

    /**
     * @inheritDoc
     */
    public function log($command): bool
    {
        return (bool)($this->saveToStorage($command));
    }

    /**
     * @inheritDoc
     */
    public function clearAll(): bool
    {
        $truncate_db = History::query()->truncate();
        if($truncate_db) {
            try {
                $new_json = json_encode([]);
                if(file_put_contents($this->file, $new_json)) {
                    return true;
                }
            } catch (\Exception $e) {
                return false;
            }
        }
        return false;
    }

    protected function saveToStorage($command)
    {
        $history = History::create($command);
        if($history) {
            if($this->saveToFile($history)) {
                return true;
            }
        }
        return false;
    }

    protected function saveToFile($data)
    {
        try {
            $current_json_data = file_get_contents($this->file);
            $current_data = json_decode($current_json_data, true);

            // Push new data
            array_push($current_data, $data);
            $new_json = json_encode($current_data, JSON_PRETTY_PRINT);
            if(file_put_contents($this->file, $new_json)) {
                return true;
            }
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

    protected function findFromDatabase($columns, $commands)
    {
        $histories = History::query();
        $histories->select($columns);
        if($commands) {
            $histories->whereIn('command', array_map('ucfirst', $commands));
        }
        $result = $histories->get();
        return collect($result->toArray())->map(function ($item, $key) {
            return array_merge(["no" => ($key + 1)], $item);
        })->all();
    }

    protected function findFromFile($columns, $commands)
    {
        try {
            $histories_json_data = file_get_contents($this->file);
            $histories = collect(json_decode($histories_json_data, true))
                ->map(function ($item) use ($columns) {
                    return collect($item)
                        ->only($columns)
                        ->all();
                })->values();

            if($commands) {
                $histories = $histories->whereIn(
                    'command', array_map('ucfirst', $commands))->values();
            }
            $data = $histories->map(function ($item) use ($columns) {
                return array_replace(array_flip($columns), $item);
            })->values();

            return $data->map(function ($item, $key) {
                return array_merge(["no" => ($key + 1)], $item);
            })->all();
        } catch (\Exception $e) {
            return [];
        }
    }
}

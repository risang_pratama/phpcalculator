<?php


namespace Jakmall\Recruitment\Calculator\Commands;


use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class CalculatorCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $commandVerb;

    /**
     * @var string
     */
    protected $commandPassiveVerb;

    /**
     * @var string
     */
    protected $operator;

    public function __construct()
    {
        $this->signature = $this->generateCommandSignature();
        $this->description = $this->generateCommandDescription();
        parent::__construct();
    }

    public function handle(CommandHistoryManagerInterface $history): void
    {
        $inputs = $this->getInputs();
        $description = $this->generateCalculationDescription($inputs);
        $result = $this->calculateAll($inputs);
        $output = sprintf('%s = %s', $description, $result);

        $history->log([
            'command' => ucfirst($this->commandVerb),
            'description' => $description,
            'result' => $result,
            'output' => $output
        ]);

        $this->comment($output);
    }

    protected function getInputs()
    {
        return $this->argument('numbers');
    }

    protected function generateCommandSignature(): string
    {
        return sprintf(
            '%s {numbers* : The numbers to be %s}',
            $this->commandVerb,
            $this->commandPassiveVerb
        );
    }

    protected function generateCommandDescription(): string
    {
        return sprintf('%s all given Numbers', ucfirst($this->commandVerb));
    }

    protected function generateCalculationDescription(array $inputs): string
    {
        $glue = sprintf(' %s ', $this->operator);
        return implode($glue, $inputs);
    }

    /**
     * @param array $numbers
     *
     * @return float|int
     */
    protected function calculateAll(array $numbers)
    {
        $number = array_pop($numbers);

        if (empty($numbers)) {
            return $number;
        }
        return $this->calculate($this->calculateAll($numbers), $number);
    }
}

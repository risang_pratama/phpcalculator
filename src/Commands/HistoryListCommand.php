<?php


namespace Jakmall\Recruitment\Calculator\Commands;


use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class HistoryListCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description = "Show calculator history";

    public function __construct()
    {
        $command_verb = "history:list";
        $command_desc = "Filter the history by commands";
        $option_desc = "Driver for storage connection";
        $this->signature = sprintf(
            '%s {commands?* : %s} {--D|driver=database : %s}',
            $command_verb, $command_desc, $option_desc
        );

        parent::__construct();
    }

    public function handle(CommandHistoryManagerInterface $history): void
    {
        $commands = $this->argument('commands');
        $driver = $this->option('driver');

        $data = $history->findAll($driver, $commands);
        if(!empty($data)) {
            $headers = ['No', 'Command', 'Description', 'Result', 'Output', 'Time'];
            $this->table($headers, $data);
        } else {
            $this->comment("History is empty");
        }
    }
}

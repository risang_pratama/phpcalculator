<?php


namespace Jakmall\Recruitment\Calculator\Commands;


class MultiplyCommand extends CalculatorCommand
{
    /**
     * @var string
     */
    protected $commandVerb = 'multiply';

    /**
     * @var string
     */
    protected $commandPassiveVerb = 'multiplied';

    /**
     * @var string
     */
    protected $operator = '*';

    /**
     * @param int|float $number1
     * @param int|float $number2
     *
     * @return int|float
     */
    protected function calculate($number1, $number2)
    {
        return $number1 * $number2;
    }
}

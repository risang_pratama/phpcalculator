<?php


namespace Jakmall\Recruitment\Calculator\Commands;


use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class HistoryClearCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = "history:clear";

    /**
     * @var string
     */
    protected $description = "Clear saved history";

    public function handle(CommandHistoryManagerInterface $history): void
    {
        $history->clearAll();
        $this->comment("History cleared!");
    }
}

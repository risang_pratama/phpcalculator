<?php


namespace Jakmall\Recruitment\Calculator\Commands;


class PowCommand extends CalculatorCommand
{
    /**
     * @var string
     */
    protected $commandVerb = 'pow';

    /**
     * @var string
     */
    protected $commandPassiveVerb = 'exponent';

    /**
     * @var string
     */
    protected $operator = '^';

    protected function generateCommandSignature(): string
    {
        return sprintf(
            '%s {base : The base number} {exp : The exponent number}',
            $this->commandVerb
        );
    }

    protected function generateCommandDescription(): string
    {
        return sprintf('%s the given number', ucfirst($this->commandPassiveVerb));
    }

    protected function generateCalculationDescription(array $arguments): string
    {
        return sprintf('%s %s %s', $arguments['base'], $this->operator, $arguments['exponent']);
    }

    protected function getInputs()
    {
        return [
            'base' => $this->argument('base'),
            'exponent' => $this->argument('exp')
        ];
    }

    /**
     * @param array $arguments
     *
     * @return float|int
     */
    protected function calculateAll(array $arguments)
    {
        $base = $arguments['base'];
        $exponent = $arguments['exponent'];

        return $this->calculate($base, $exponent);
    }

    /**
     * @param int|float $base
     * @param int|float $exponent
     *
     * @return int|float
     */
    protected function calculate($base, $exponent)
    {
        return pow($base, $exponent);
    }
}

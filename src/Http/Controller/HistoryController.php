<?php

namespace Jakmall\Recruitment\Calculator\Http\Controller;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Jakmall\Recruitment\Calculator\DBConnection;
use Jakmall\Recruitment\Calculator\Model\History;

class HistoryController
{
    private $delimiter;

    public function __construct()
    {
        $this->delimiter = [
            'add' => ' + ',
            'subtract' => ' - ',
            'multiply' => ' * ',
            'divide' => ' / ',
            'pow' => ' ^ '
        ];
        new DBConnection();
    }

    public function index()
    {
        $data = [];
        $histories = History::all();
        if(!empty($histories)) {
            foreach ($histories as $history) {
                $data[] = $this->mappingHistoryData($history);
            }
        }
        return JsonResponse::create($data);
    }

    public function show($id)
    {
        try {
            $history = History::findOrFail($id);
            return JsonResponse::create($this->mappingHistoryData($history));
        } catch (ModelNotFoundException $e) {
            return Response::create('', 404);
        }
    }

    public function remove($id)
    {
        $file = __DIR__."/../../../database/storage/calculator.json";
        try {
            $history = History::findOrFail($id);
            $history->delete();

            $histories_json_data = file_get_contents($file);
            $histories = collect(json_decode($histories_json_data, true))
                ->filter(function ($item) use ($id) {
                    return $item['id'] <> $id;
            })->values()->toJson();
            file_put_contents($file, $histories);

            return Response::create('', 204);
        } catch (ModelNotFoundException $e) {
            return Response::create('', 404);
        }
    }

    /**
     * @param History $history
     *
     * @return array
     */
    protected function mappingHistoryData(History $history): array
    {
        $command = strtolower($history->command);
        $input = explode($this->delimiter[$command], $history->description);
        return [
            'id' => "id".$history->id,
            'command' => $command,
            'operation' => $history->description,
            'input' => array_map('floatval', $input),
            'result' => (float)$history->result,
            'time' => date('Y-m-d H:i:s', strtotime($history->created))
        ];
    }
}

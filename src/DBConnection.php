<?php


namespace Jakmall\Recruitment\Calculator;


use Illuminate\Database\Capsule\Manager as Capsule;

class DBConnection
{
    function __construct()
    {
        $database_config = require_once __DIR__."/../config/database.php";

        $capsule = new Capsule();

        $capsule->addConnection([
            'driver'    => $database_config['db_connection'],
            'host'      => $database_config['db_host'],
            'database'  => $database_config['db_name'],
            'username'  => $database_config['db_user'],
            'password'  => $database_config['db_password'],
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ]);

        $capsule->setAsGlobal();
        $capsule->bootEloquent();
    }
}

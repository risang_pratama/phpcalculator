<?php

define('DB_CONNECTION', 'sqlite');
define('DB_HOST', 'localhost');
define('DB_NAME', __DIR__.'/../database/storage/calculator.sqlite.db');
define('DB_USER', '');
define('DB_PASSWORD', '');

return [
    'db_connection' => DB_CONNECTION,
    'db_host' => DB_HOST,
    'db_name' => DB_NAME,
    'db_user' => DB_USER,
    'db_password' => DB_PASSWORD
];

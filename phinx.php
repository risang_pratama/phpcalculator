<?php

return
[
    'paths' => [
        'migrations' => 'database/migrations'
    ],
    'environments' => [
        'default_migration_table' => 'phinxlog',
        'default_environment' => 'development',
        'development' => [
            'adapter' => 'sqlite',
            'name' => 'database/storage/calculator.sqlite',
            'suffix' => '.db'
        ]
    ],
    'version_order' => 'creation'
];
